#include <cstring>
#include "Game.h"
#include "PacketId.h"
#include "GameManager.h"

const sf::Time Game::timePerFrame = sf::seconds(1.f / 60.f);

Game::Game(GameManager& gameManager, bool isFirstPlayer) : ball(ballRadius), ballVelocity(sf::Vector2f(ballSpeed, 0.f)), leftBat(sf::Vector2f(batWidth, batHeight)), rightBat(sf::Vector2f(batWidth, batHeight)),
	gameManager(gameManager), isFirstPlayer(isFirstPlayer)
{
	
	window.create(sf::VideoMode(screenWidth, screenHeight), "Pong");
	window.setVerticalSyncEnabled(true);
	
	sprintf(scoreChar, "%d : %d", scoreLeft, scoreRight);
	
	font.loadFromFile("../../data/arial.ttf");
	
	scoreBoard.setFont(font);
	scoreBoard.setFillColor(sf::Color::White);
	scoreBoard.setString(static_cast<std::string>(scoreChar));
	scoreBoard.setPosition(screenWidth / 2.f - scoreBoard.getGlobalBounds().width / 2.f, 10.f);
	
	ballSpeedText.setFont(font);
	ballSpeedText.setString("Speed: " + std::to_string(static_cast<int>(ballSpeed / 10)));
	ballSpeedText.setFillColor(sf::Color::White);
	ballSpeedText.setCharacterSize(20);
	ballSpeedText.setPosition(screenWidth / 2.f - ballSpeedText.getGlobalBounds().width / 2.f, screenHeight - 50.f);
	
	
	ball.setFillColor(sf::Color::Red);
	leftBat.setFillColor(sf::Color::White);
	rightBat.setFillColor(sf::Color::White);
	
	ball.setPosition(sf::Vector2f(screenWidth / 2.f, screenHeight / 2.f));
	leftBat.setPosition(sf::Vector2f(0.f, screenHeight / 2.f - batHeight / 2.f + 2));
	rightBat.setPosition(sf::Vector2f(screenWidth - batWidth, screenHeight / 2.f - batHeight / 2.f + 2));
}

void Game::run()
{
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (window.isOpen())
	{
		handleEvents();
		timeSinceLastUpdate += clock.restart();
		while (timeSinceLastUpdate > timePerFrame)
		{
			handleEvents();
			timeSinceLastUpdate -= timePerFrame;
			update(timePerFrame);
		}
		draw();
	}
	
}

void Game::handleEvents()
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			leftMove = 1;
			
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			leftMove = 2;
			
		}
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			rightMove = 1;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
		{
			rightMove = 2;
			
		}
		
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::W) && !sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			leftMove = 0;
		
		if (!sf::Keyboard::isKeyPressed(sf::Keyboard::Up) && !sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			rightMove = 0;
		
		if (event.type == sf::Event::Closed)
			window.close();
	}
}

void Game::update(sf::Time deltaTime)
{
	//Left bat
	//paddle center
	if (ball.getGlobalBounds().intersects(leftBat.getGlobalBounds()) && ball.getGlobalBounds().top == leftBat.getGlobalBounds().top + leftBat.getGlobalBounds().height / 2.f)
	{
		if (ballSpeed == 300.)
			ballSpeed = 500.;
		ballVelocity = sf::Vector2f(ballSpeed, 0.f);
		ballSpeed += 200;
		ball.setFillColor(sf::Color::Yellow);
	}
		//paddle top
	else if (ball.getGlobalBounds().intersects(leftBat.getGlobalBounds()) && ball.getGlobalBounds().top < leftBat.getGlobalBounds().top + batHeight / 2.f)
	{
		if (ballSpeed == 300.)
			ballSpeed = 500.;
		float ballHitPoint = (leftBat.getGlobalBounds().top + (leftBat.getGlobalBounds().height / 2.f)) - (ball.getGlobalBounds().top + (ball.getGlobalBounds().height / 2.f));
		ballVelocity = sf::Vector2f(ballSpeed, -ballHitPoint * 2.f);
		ballSpeed += 10.;
	}
		//paddle bot
	else if (ball.getGlobalBounds().intersects(leftBat.getGlobalBounds()) && ball.getGlobalBounds().top > leftBat.getGlobalBounds().top + batHeight / 2.f)
	{
		if (ballSpeed == 300)
			ballSpeed = 500;
		float ballHitPoint = (leftBat.getGlobalBounds().top + (leftBat.getGlobalBounds().height / 2.f)) - (ball.getGlobalBounds().top + (ball.getGlobalBounds().height / 2.f));
		ballVelocity = sf::Vector2f(ballSpeed, ballHitPoint * -2.f);
		ballSpeed += 10.;
	}
	
	
	//Right bat
	//paddle center
	if (ball.getGlobalBounds().intersects(rightBat.getGlobalBounds()) && ball.getGlobalBounds().top == rightBat.getGlobalBounds().top + rightBat.getGlobalBounds().height / 2.f)
	{
		if (ballSpeed == 300.)
			ballSpeed = 500.;
		ballVelocity = sf::Vector2f(-ballSpeed, 0.f);
		ballSpeed += 200.;
		ball.setFillColor(sf::Color::Yellow);
		
	}
	else if (ball.getGlobalBounds().intersects(rightBat.getGlobalBounds()) && ball.getGlobalBounds().top < rightBat.getGlobalBounds().top + batHeight / 2.f)
	{
		if (ballSpeed == 300.)
			ballSpeed = 500.;
		float ballHitPoint = (rightBat.getGlobalBounds().top + (rightBat.getGlobalBounds().height / 2.f)) - (ball.getGlobalBounds().top + (ball.getGlobalBounds().height / 2.f));
		ballVelocity = sf::Vector2f(-ballSpeed, -ballHitPoint * 2.f);
		ballSpeed += 10.;
		
	}
		//Paddle bot
	else if (ball.getGlobalBounds().intersects(rightBat.getGlobalBounds()) && ball.getGlobalBounds().top > rightBat.getGlobalBounds().top + batHeight / 2.f)
	{
		if (ballSpeed == 300)
			ballSpeed = 500;
		float ballHitPoint = (rightBat.getGlobalBounds().top + (rightBat.getGlobalBounds().height / 2.f)) - (ball.getGlobalBounds().top + (ball.getGlobalBounds().height / 2.f));
		ballVelocity = sf::Vector2f(-ballSpeed, ballHitPoint * -2.f);
		ballSpeed += 10.;
		
	}
	
	//Top side bounce
	if (ball.getGlobalBounds().top <= 0)
	{
		ballVelocity.y *= -1.f;
	}
		//Bottom side bounce
	else if (ball.getGlobalBounds().top + ball.getGlobalBounds().height >= screenHeight)
	{
		ballVelocity.y *= -1.f;
	}
	
	//Left side collision
	if (ball.getGlobalBounds().left <= 0)
	{
		scoreRight++;
		sprintf(scoreChar, "%d : %d", scoreLeft, scoreRight);
		scoreBoard.setString(static_cast<std::string>(scoreChar));
		ball.setPosition(sf::Vector2f(screenWidth / 2.f, screenHeight / 2.f));
		ball.setFillColor(sf::Color::Red);
		ballSpeed = 300;
		ballVelocity = sf::Vector2f(-ballSpeed, 0.f);
		
	}
	//Right side collision
	if (ball.getGlobalBounds().left + ball.getGlobalBounds().width >= screenWidth)
	{
		scoreLeft++;
		sprintf(scoreChar, "%d : %d", scoreLeft, scoreRight);
		scoreBoard.setString(static_cast<std::string>(scoreChar));
		ball.setPosition(sf::Vector2f(screenWidth / 2.f, screenHeight / 2.f));
		ball.setFillColor(sf::Color::Red);
		ballSpeed = 300;
		ballVelocity = sf::Vector2f(ballSpeed, 0.f);
	}
	
	
	ball.move(ballVelocity.x * deltaTime.asSeconds(), ballVelocity.y * deltaTime.asSeconds());
	
	//paddle move
	
	if (leftMove == 1 && isFirstPlayer)
	{
		if (leftBat.getPosition().y > 0)
			leftBat.move(0.f, -paddleSpeed * deltaTime.asSeconds());
	}
	else if (leftMove == 2 && isFirstPlayer)
	{
		if (leftBat.getPosition().y + rightBat.getGlobalBounds().height < screenHeight)
			leftBat.move(0.f, paddleSpeed * deltaTime.asSeconds());
	}
	
	
	if (rightMove == 1 && !isFirstPlayer)
	{
		if (rightBat.getPosition().y > 0)
			rightBat.move(0.f, -paddleSpeed * deltaTime.asSeconds());
	}
	else if (rightMove == 2 && !isFirstPlayer)
	{
		if (rightBat.getPosition().y + rightBat.getGlobalBounds().height < screenHeight)
			rightBat.move(0.f, paddleSpeed * deltaTime.asSeconds());
	}
	
	
	ballSpeedText.setString("Speed: " + std::to_string(static_cast<int>(ballSpeed / 10)));
}

void Game::draw()
{
	// send data about game state
	
	std::vector<uint8_t> buffer;
	buffer.resize(sizeof(uint32_t) + sizeof(PacketId) + sizeof(sf::Vector2<float>));
	uint32_t packetSize = sizeof(PacketId) + sizeof(sf::Vector2<float>);
	PacketId id = PacketId::GAME_UPDATE;
	std::memcpy(buffer.data(), &packetSize, sizeof(decltype(packetSize)));
	std::memcpy(buffer.data() + sizeof(decltype(packetSize)), &id, sizeof(decltype(id)));
	
	if (isFirstPlayer)
	{
		auto leftPosition = leftBat.getPosition();
		std::memcpy(buffer.data() + sizeof(decltype(packetSize)) + sizeof(decltype(id)), &leftPosition, sizeof(decltype(leftPosition)));
	}
	else
	{
		auto rightPosition = rightBat.getPosition();
		std::memcpy(buffer.data() + sizeof(decltype(packetSize)) + sizeof(decltype(id)), &rightPosition, sizeof(decltype(rightPosition)));
	}
	gameManager.Send(buffer);
	
	
	window.clear();
	window.draw(ballSpeedText);
	window.draw(scoreBoard);
	window.draw(leftBat);
	window.draw(rightBat);
	window.draw(ball);
	window.display();
}
