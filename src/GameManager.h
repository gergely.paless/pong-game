#pragma once

#include "Game.h"
#include "PacketId.h"

#include "asio.hpp"

#include <thread>
#include <vector>
#include <deque>

class GameManager
{
public:
	GameManager() : m_Socket(m_Service)
	{
		Connect();
		
		
		// launch service thread
		m_ServiceThread = std::thread([this]() {
			m_Service.run();
		});
	}
	
	~GameManager()
	{
		m_ServiceThread.join();
	}
	
	void Run()
	{
		game = std::make_unique<Game>(*this, isFirstPlayer);
		game->run();
	}
	
	void Login(const std::string& username, const std::string& password)
	{
		std::vector<uint8_t> sendBuffer;
		auto type = static_cast<std::size_t>(PacketId::LOGIN);
		uint32_t packetSize = sizeof(decltype((type))) + username.size() + 1 + password.size() + 1;
		sendBuffer.resize(packetSize + sizeof(decltype(packetSize)));
		
		std::memcpy(sendBuffer.data(), &packetSize, sizeof(decltype(packetSize)));
		std::memcpy(sendBuffer.data() + sizeof(decltype(packetSize)), &type, sizeof(decltype((type))));
		std::memcpy(sendBuffer.data() + sizeof(decltype(packetSize)) + sizeof(decltype((type))), username.data(), username.size());
		std::memcpy(sendBuffer.data() + sizeof(decltype(packetSize)) + sizeof(decltype((type))) + username.size(), "\0", 1);
		std::memcpy(sendBuffer.data() + sizeof(decltype(packetSize)) + sizeof(decltype((type))) + username.size() + 1, password.data(), password.size());
		std::memcpy(sendBuffer.data() + sizeof(decltype(packetSize)) + sizeof(decltype((type))) + username.size() + 1 + password.size(), "\0", 1);
		
		std::cout << "login packet is: " << sendBuffer.data() << std::endl;
		
		Send(sendBuffer);
	}
	
	void Register(const std::string& username, const std::string& password)
	{
		std::vector<uint8_t> sendBuffer;
		auto type = static_cast<std::size_t>(PacketId::REGISTER);
		uint32_t packetSize = sizeof(decltype((type))) + username.size() + 1 + password.size() + 1;
		sendBuffer.resize(packetSize + sizeof(decltype(packetSize)));
		
		std::memcpy(sendBuffer.data(), &packetSize, sizeof(decltype(packetSize)));
		std::memcpy(sendBuffer.data() + sizeof(decltype(packetSize)), &type, sizeof(decltype((type))));
		std::memcpy(sendBuffer.data() + sizeof(decltype(packetSize)) + sizeof(decltype((type))), username.data(), username.size());
		std::memcpy(sendBuffer.data() + sizeof(decltype(packetSize)) + sizeof(decltype((type))) + username.size(), "\0", 1);
		std::memcpy(sendBuffer.data() + sizeof(decltype(packetSize)) + sizeof(decltype((type))) + username.size() + 1, password.data(), password.size());
		std::memcpy(sendBuffer.data() + sizeof(decltype(packetSize)) + sizeof(decltype((type))) + username.size() + 1 + password.size(), "\0", 1);
		
		std::cout << "register packet is: " << sendBuffer.data() << std::endl;
		
		Send(sendBuffer);
	}

public:
	void Process()
	{
		if (buffer.empty())
			return;
		
		switch (PacketId{*(buffer.data() + sizeof(uint32_t))})
		{
			case PacketId::PING:
				SendPing();
				break;
				
			case PacketId::PINGED:
				// ok, acknowledged
				std::cout << "Server sent the PINGED message back" << std::endl;
				break;
				
			case PacketId::LOGGED_IN:
				// ok, acknowledged
				std::cout << "Server sent the LOGGED_IN message back" << std::endl;
				break;
				
			case PacketId::REGISTERED:
				// ok, acknowledged
				std::cout << "Server sent the REGISTERED message back" << std::endl;
				break;
				
			case PacketId::GAME_CREATED:
				// ok, acknowledged
				std::cout << "Server sent the GAME_CREATED message back" << std::endl;
				isFirstPlayer = *(buffer.data() + sizeof(uint32_t) + sizeof(PacketId));
				m_GameCreatedCondVar.notify_all();
				break;
			
			case PacketId::GAME_UPDATE:
				{
					if (!game)
						break;
					
					sf::Vector2f opponentBatPosition = *reinterpret_cast<sf::Vector2f*>(buffer.data() + sizeof(uint32_t) + sizeof(PacketId));
					if (isFirstPlayer)
					{
						game->rightBat.setPosition(opponentBatPosition);
					}
					else
					{
						game->leftBat.setPosition(opponentBatPosition);
					}
				}
				break;
			
			default:
				std::cout << "Unknown PacketId, ignoring this packet" << std::endl;
				return;
		}
	}
	
	void SendPing()
	{
		std::vector<uint8_t> sendBuffer;
		sendBuffer.resize(sizeof(uint32_t) + sizeof(PacketId));
		uint32_t packetSize = sizeof(PacketId);
		PacketId id = PacketId::PING;
		std::memcpy(sendBuffer.data(), &packetSize, sizeof(decltype(packetSize)));
		std::memcpy(sendBuffer.data() + sizeof(decltype(packetSize)), &id, sizeof(PacketId));
		
		Send(sendBuffer);
	}
	
	void Connect()
	{
		m_Socket.async_connect(asio::ip::tcp::endpoint(asio::ip::make_address("127.0.0.1"), 3333),
		                       [this](std::error_code ec) {
			                       if (!ec)
			                       {
				                       ReadHeader();
			                       }
			                       else
			                       {
				                       throw std::system_error(ec);
			                       }
		                       });
	}
	
	void ReadHeader()
	{
		asio::async_read(m_Socket, asio::buffer(&bufferSize, sizeof(decltype(bufferSize))),
		                 [this](std::error_code ec, std::size_t length) {
			                 if (!ec)
			                 {
				                 ReadBody();
			                 }
			                 else
			                 {
				                 m_Socket.close();
								 std::cout << "read header fail, endpoint probably closed the connection. Error: " << ec << std::endl;
			                 }
		                 });
	}
	
	void ReadBody()
	{
		buffer.resize(bufferSize);
		asio::async_read(m_Socket,
		                 asio::buffer(buffer),
		                 [this](std::error_code ec, std::size_t length) {
			                 if (!ec)
			                 {
								 std::cout << "packet received, size is: " << buffer.size() << std::endl;
				                 ReadHeader();
								 Process();
			                 }
			                 else
			                 {
				                 std::cout << "read body fail, endpoint probably closed the connection. Error: " << ec << std::endl;
				                 m_Socket.close();
			                 }
		                 });
	}
	
	void Send(const std::vector<uint8_t>& sendBuffer)
	{
		std::scoped_lock<std::mutex> lock(m_OutgoingMessagesMutex);
		bool isOutMessagesEmpty = m_OutMessages.empty();
		m_OutMessages.push_back(sendBuffer);
		if (isOutMessagesEmpty)
		{
			Write();
		}
	}
	
	void Write()
	{
		asio::async_write(m_Socket, asio::buffer(m_OutMessages.front().data(), m_OutMessages.front().size()),
		                  [this](std::error_code ec, std::size_t length) {
			                  if (!ec)
			                  {
								  std::scoped_lock<std::mutex> lock(m_OutgoingMessagesMutex);
				                  m_OutMessages.pop_front();
				                  if (!m_OutMessages.empty())
				                  {
					                  Write();
				                  }
			                  }
			                  else
			                  {
				                  std::cout << "write fail, endpoint probably closed the connection. Error: " << ec << std::endl;
				                  m_Socket.close();
			                  }
		                  });
	}

public:
	std::unique_ptr<Game> game;
	
	
	asio::io_service m_Service;
	std::thread m_ServiceThread;
	
	asio::ip::tcp::socket m_Socket;
	
	std::deque<std::vector<uint8_t>> m_OutMessages;
	std::mutex m_OutgoingMessagesMutex;
	
	std::vector<uint8_t> buffer;
	size_t bufferSize {};
	size_t headerLength = 8;
	
	std::condition_variable m_GameCreatedCondVar;
	std::mutex m_GameCreatedMutex;

	bool isFirstPlayer = true;
	
};
