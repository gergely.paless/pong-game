#include "Game.h"

#include "asio.hpp"
#include "Menu.h"


int main()
{
	//std::cout << "GAME_UPDATE packet size: " << sizeof(uint32_t) + sizeof(PacketId) + sizeof(sf::Vector2<float>) << std::endl;
	
	Menu menu;
	menu.Show();
	
	std::unique_lock<std::mutex> lock(menu.gameManager.m_GameCreatedMutex);
	menu.gameManager.m_GameCreatedCondVar.wait(lock);
	
	menu.gameManager.Run();
	
	std::cin.get();
	return 0;
}