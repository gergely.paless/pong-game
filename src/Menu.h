#pragma once

#include <iostream>
#include "GameManager.h"

class Menu
{
public:
	void Show()
	{
		std::cout << "Please log in or register: " << std::endl;
		std::cout << "1.\tLogin" << std::endl;
		std::cout << "2.\tRegister" << std::endl;
		std::cout << ">>>> " << std::flush;
		
		int input;
		std::cin >> input;
		if (input == 1)
			Login();
		else if (input == 2)
			Register();
	}
	
private:
	void Login()
	{
		std::cout << "User name: " << std::flush;
		std::string username;
		std::cin >> username;
		
		std::cout << "Password: " << std::flush;
		std::string password;
		std::cin >> password;
		
		gameManager.Login(username, password);
	}

	void Register()
	{
		std::cout << "User name: " << std::flush;
		std::string username;
		std::cin >> username;
		
		std::cout << "Password: " << std::flush;
		std::string password;
		std::cin >> password;
		
		std::cout << "Password again: " << std::flush;
		std::string passwordAgain;
		std::cin >> passwordAgain;
		
		if (password != passwordAgain)
		{
			std::cout << "password does not match" << std::endl;
			return;
		}
		
		gameManager.Register(username, password);
	}
	
public:
	GameManager gameManager;
};
