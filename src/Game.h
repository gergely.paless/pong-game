#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>
#include <cmath>

class GameManager;

class Game
{
public:
	Game(GameManager& gameManager, bool isFirstPlayer);
	void run();
	
public:
	void handleEvents();
	void update(sf::Time deltaTime);
	void draw();

	static const sf::Time timePerFrame;
	const int screenWidth{ 800 };
	const int screenHeight{ 500 };
	const float batWidth{ 30.f };
	const float batHeight{ 100. };
	const float ballRadius{ 12.f };
	float ballSpeed{ 300 };
	const float paddleSpeed{ 300.f };

	int leftMove{ 0 };
	int rightMove{ 0 };

	sf::RenderWindow window;
	
	int scoreLeft{ 0 };
	int scoreRight{ 0 };
	char scoreChar[20];

	sf::Font font;
	sf::Text scoreBoard;
	sf::Text ballSpeedText;

	sf::Vector2f ballVelocity;

	sf::CircleShape ball;
	sf::RectangleShape leftBat;
	sf::RectangleShape rightBat;
	
	bool isFirstPlayer = true;
	
	GameManager& gameManager;
};
